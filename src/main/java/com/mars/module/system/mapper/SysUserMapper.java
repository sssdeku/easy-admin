package com.mars.module.system.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mars.common.request.sys.SysUserQueryRequest;
import com.mars.common.response.sys.SysUserListResponse;
import com.mars.framework.mapper.BasePlusMapper;
import com.mars.module.system.entity.SysUser;
import org.apache.ibatis.annotations.Param;

/**
 * 用户
 *
 * @author 源码字节-程序员Mars
 */
public interface SysUserMapper extends BasePlusMapper<SysUser> {


    /**
     * 分页查询
     *
     * @param page     page
     * @param queryDto queryDto
     * @return IPage<SysUserListResponse>
     */
    IPage<SysUserListResponse> selectPageList(@Param("page") IPage<Object> page, @Param("queryDto") SysUserQueryRequest queryDto);

}
