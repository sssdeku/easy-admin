package com.mars.module.tool.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.mars.module.system.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * 业务表 建表记录
 *
 * @author mars
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class GenCreateTableRecord extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 表名称
     */
    private String tableName;


    /**
     * 表名称
     */
    private String tableEnName;

    /**
     * 是否生成基础字段 0 生成 1 不生成
     */
    private Integer genBaseField;


}
