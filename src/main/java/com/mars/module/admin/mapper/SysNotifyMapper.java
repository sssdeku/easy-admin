package com.mars.module.admin.mapper;

import com.mars.module.admin.entity.SysNotify;
import com.mars.framework.mapper.BasePlusMapper;

/**
 * 通知公告Mapper接口
 *
 * @author mars
 * @date 2023-12-06
 */
public interface SysNotifyMapper extends BasePlusMapper<SysNotify> {

}
