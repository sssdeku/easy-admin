package com.mars.module.admin.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.*;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import cn.afterturn.easypoi.excel.annotation.Excel;
import java.time.LocalDateTime;
import com.mars.module.system.entity.BaseEntity;

    /**
 * 文件存储对象 sys_oss
 *
 * @author mars
 * @date 2023-11-20
 */

@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@ApiModel(value = "文件存储对象")
@Accessors(chain = true)
@TableName("sys_oss")
public class SysOss extends BaseEntity {


    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 名称
     */
    @Excel(name = "名称")
    @ApiModelProperty(value = "名称")
    private String name;

    /**
     * endpoint
     */
    @Excel(name = "endpoint")
    @ApiModelProperty(value = "endpoint")
    private String endpoint;

    /**
     * access_key
     */
    @Excel(name = "access_key")
    @ApiModelProperty(value = "access_key")
    private String accessKey;

    /**
     * secret_key
     */
    @Excel(name = "secret_key")
    @ApiModelProperty(value = "secret_key")
    private String secretKey;

    /**
     * bucket_Name
     */
    @Excel(name = "bucket_Name")
    @ApiModelProperty(value = "bucket_Name")
    private String bucketName;

    /**
     * 创建时间
     */
    @Excel(name = "创建时间")
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
}
