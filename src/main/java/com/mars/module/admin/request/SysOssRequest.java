package com.mars.module.admin.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.mars.common.request.PageRequest;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * 文件存储请求对象 sys_oss
 *
 * @author mars
 * @date 2023-11-20
 */
@Data
@ApiModel(value = "文件存储对象")
@EqualsAndHashCode(callSuper = true)
public class SysOssRequest extends PageRequest{



    /**
     * ID
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    private String name;

    /**
     * endpoint
     */
    @ApiModelProperty(value = "endpoint")
    private String endpoint;

    /**
     * access_key
     */
    @ApiModelProperty(value = "access_key")
    private String accessKey;

    /**
     * secret_key
     */
    @ApiModelProperty(value = "secret_key")
    private String secretKey;

    /**
     * bucket_Name
     */
    @ApiModelProperty(value = "bucket_Name")
    private String bucketName;

    /**
     * 创建时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
}
