package com.mars.module.oss.service;

import org.springframework.web.multipart.MultipartFile;

public interface IFileService {

    /**
     * 文件上传至阿里云
     *
     * @param file 文件
     * @return String
     */
    String uploadImg(MultipartFile file);

    /**
     * 上传文件
     *
     * @param file file
     * @return String
     */
    String uploadFile(MultipartFile file);
}
