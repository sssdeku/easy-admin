package com.mars.common.request.sys;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import java.util.List;

/**
 * 角色修改DTO
 *
 * @author 源码字节-程序员Mars
 */
@Data
public class SysRoleUpdateRequest {

    @NotNull
    @ApiModelProperty(value = "ID")
    private Long id;

    @NotEmpty(message = "请输入角色名称")
    @ApiModelProperty(value = "角色名称")
    private String roleName;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "菜单id")
    private List<Long> menuId;

}
