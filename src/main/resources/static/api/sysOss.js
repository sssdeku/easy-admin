/**
 * 分页查询文件存储列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/sysOss/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询文件存储详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/sysOss/query/' + id,
        method: 'get'
    })
}

/**
 * 新增文件存储
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/sysOss/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改文件存储
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/sysOss/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除文件存储
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/sysOss/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出文件存储
 *
 * @param query
 * @returns {*}
 */
function exportOss(query) {
    return requests({
        url: '/admin/sysOss/export',
        method: 'get',
        params: query
    })
}
